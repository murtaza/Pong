# Pong - Unreal Engine

Arcade sports game Pong inspired by table tennis dynamics.

![](images/pong.gif)


## How to start the game?
1. Download the WarehouseWreckage/Windows directory
2. Extract contents into a folder
3. Open up the Windows/Pong.exe

## How to play?
Move up and down with W (up) and S (down) keys.

HINT: You can collect velocity and hold it for the next hit. 
- Tap spacebar while moving in a direction (up or down) to store the velocity.

The ball gets faster with each hit 

Hit the ball past the opponent, you start on the left
